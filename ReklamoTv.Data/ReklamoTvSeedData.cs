﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data
{
    public class ReklamoTvSeedData : DropCreateDatabaseIfModelChanges<ReklamoTvEntities>
    {
        protected override void Seed(ReklamoTvEntities context)
        {
            GetAboutUses().ForEach(a => context.AboutUses.Add(a));
            GetNewses().ForEach(n => context.Newses.Add(n));
            GetTags().ForEach(t => context.Tags.Add(t));
            GetProduct().ForEach(p => context.Products.Add(p));
            GetReferance().ForEach(r => context.Referances.Add(r));
            GetContact().ForEach(c => context.Contacts.Add(c));
            GetContactForm1().ForEach(f => context.ContactForm1s.Add(f));
        }
        private List<AboutUs> GetAboutUses()
        {
            return new List<AboutUs>
                {
                    new AboutUs
                    {
                        Description="ReklamoTv"
                     }

                };
        }
        private List<News> GetNewses()
        {
            return new List<News>
                    {
                       new News
                       {
                           NewsTitle="",
                           Description="",

                        }
                    };
        }
        private List<Tag> GetTags()
        {
            return new List<Tag>
                {
                    new Tag
                    {
                        DescriptionTag="",
                        TagTitle="",
                        Tags=""
                    }
                };
        }
        private List<Product> GetProduct()
        {
            return new List<Product>
                {
                    new Product
                    {
                        Contents="",
                        Title="",
                        Description=""
                   }

                };

        }
        private List<Referance> GetReferance()
        {
            return new List<Referance>
                {
                    new Referance
                    {
                        ReferanceTitle="",
                        Referanceİmage="",
                        Description=""
                    }
                };
        }
        private List<Contact> GetContact()
        {
            return new List<Contact>
                {
                    new Contact
                    {
                         ContactAdress="",
                         ContactPhone="",
                         FacebookAdress="",
                         FirstEMail="",
                         GoogleAdress="",
                         Latitude="",
                         LinkedAdress="",
                         Longtitude="",
                         SecondEMail="",
                         SkypeAdress="",
                         TwitterAdress=""

                    }

                };
        }
        private List<ContactForm1> GetContactForm1()
        {
            return new List<ContactForm1>
                {
                    new ContactForm1
                    {
                        ContactName= "",
                        ContactSurname="",
                        ContactMesaj="",
                        ContactMail="",
                        ContactTelefon="",

                    }

                };
        }


    }
}
