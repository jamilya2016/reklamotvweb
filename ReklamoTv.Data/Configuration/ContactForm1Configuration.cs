﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
     public class ContactForm1Configuration: EntityTypeConfiguration<ContactForm1>
    {
        public ContactForm1Configuration()
        {
            ToTable("ContactForm1");
            HasKey<int>(f=>f.ContactFormId);
            Property(f=>f.ContactName);
            Property(f=>f.ContactSurname);
            Property(f => f.ContactTelefon);
            Property(f => f.ContactMesaj);
            Property(f => f.ContactMail);

        }
    }
}
