﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
     public class ProductConfiguration:EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            ToTable("Products");
            HasKey<int>(p => p.ProductId);
            Property(p => p.Contents);
            Property(p => p.Description);
            Property(p => p.Title);


        }
    }
}
