﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
     public class NewsConfiguration:EntityTypeConfiguration<News>
    {
        public NewsConfiguration()
        {
            ToTable("Newses");
            HasKey<int>(n => n.NewsId);
            Property(n => n.NewsTime);
            Property(n => n.Description);
            Property(n => n.NewsTitle);
            Property(n => n.NewsSpot);
            Property(n => n.StartTime);
            Property(n => n.ExpiryTime);



        }
    }
}
