﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
   public class AboutUsConfiguration : EntityTypeConfiguration<AboutUs>
    {
        public AboutUsConfiguration()
        {
            ToTable("AboutUs");
            HasKey<int>(a=>a.AboutId);
            Property(a => a.Description);
            Property(a => a.AboutTitle);

        }

       
    }
}
