﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
    public class ContactConfiguration : EntityTypeConfiguration<Contact>
    {
        public ContactConfiguration()
        {
            ToTable("Contact");
            HasKey<int>(c => c.ContactId);
            Property(c => c.ContactAdress);
            Property(c => c.ContactPhone);
            Property(c => c.FacebookAdress);
            Property(c => c.FirstEMail);
            Property(c => c.GoogleAdress);
            Property(c => c.Latitude);
            Property(c => c.LinkedAdress);
            Property(c => c.Longtitude);
            Property(c => c.SecondEMail);
            Property(c => c.SkypeAdress);
            Property(c => c.TwitterAdress);



        }


    }
}
