﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
    class ReferanceConfiguration : EntityTypeConfiguration<Referance>
    {
        public ReferanceConfiguration()
        {
            ToTable("Referance");
            HasKey<int>(r => r.ReferanceId);
            Property(r => r.Description);
            Property(r => r.ReferanceTitle);
            Property(r => r.Referanceİmage);

        }
    }
}
