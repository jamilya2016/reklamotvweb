﻿using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Configuration
{
    public class TagConfiguration : EntityTypeConfiguration<Tag>
    {
        public TagConfiguration()
        {
            ToTable("Tag");
            HasKey<int>(t => t.TagId);
            Property(t => t.TagTitle);
            Property(t => t.DescriptionTag);
            Property(t => t.Tags);


        }
    }
}
