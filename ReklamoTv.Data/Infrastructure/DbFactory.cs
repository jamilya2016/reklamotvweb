﻿using ReklamoTv.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Infrastructure
{
    public class DbFactory:Disposable,IDbFactory
    {
        ReklamoTvEntities dbContext;

        public ReklamoTvEntities Init()
        {
            return dbContext ?? (dbContext = new ReklamoTvEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
