﻿using ReklamoTv.Data.Configuration;
using ReklamoTv.Model;
using ReklamoTv.Model;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data
{
    public  class ReklamoTvEntities:DbContext
    {
        public ReklamoTvEntities() : base("ReklamoTvEntities") {
        }

        public static ReklamoTvEntities Create()
        {

            return new  ReklamoTvEntities();
        }

        public DbSet<AboutUs>AboutUses{ get; set; }
        public DbSet<Contact>Contacts { get; set; }
        public DbSet<News>Newses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Referance> Referances { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<ContactForm1> ContactForm1s{ get; set; }




        public virtual void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new AboutUsConfiguration());
            modelBuilder.Configurations.Add(new ContactConfiguration());
            modelBuilder.Configurations.Add(new NewsConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new ReferanceConfiguration());
            modelBuilder.Configurations.Add(new TagConfiguration());
            modelBuilder.Configurations.Add(new ContactForm1Configuration());

            Database.SetInitializer<ReklamoTvEntities>(null);

            base.OnModelCreating(modelBuilder);
        }
    }
}

