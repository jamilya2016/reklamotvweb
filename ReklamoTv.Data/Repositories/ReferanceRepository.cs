﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{
     public class ReferanceRepository:RepositoryBase<Referance>,IReferanceRepository
    {

    
    public ReferanceRepository(IDbFactory dbFactory)  
                : base(dbFactory) { }

    public IEnumerable<Referance> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
    {
        search = search.Trim();
        var searchWords = search.Split(' ');


        var query = this.DbContext.Referances.AsQueryable();
        foreach (string sSearch in searchWords)
        {
            if (sSearch != null && sSearch != "")
            {
                query = query.Where(r => r.Description.Contains(sSearch));

            }
        }

        var allReferances = query;

            IEnumerable<Referance> filteredReferance = allReferances;

            if (sortDirection == "asc")
        {
            switch (sortColumnIndex)
            {
                case 1:
                    filteredReferance = filteredReferance.OrderBy(r => r.ReferanceId);
                    break;
                case 2:
                   filteredReferance  = filteredReferance.OrderBy(r => r.Description);
                    break;
                case 3:
                  filteredReferance = filteredReferance.OrderBy(r => r.ReferanceTitle);
                    break;
                case 4:
                    filteredReferance = filteredReferance.OrderBy(r => r.Referanceİmage);
                    break;
                default:
                   filteredReferance = filteredReferance.OrderBy(r=>r.ReferanceId);
                    break;
            }
        }
        else
        {
            switch (sortColumnIndex)
            {
                case 1:
                    filteredReferance = filteredReferance.OrderBy(r => r.ReferanceId);
                    break;
                case 2:
                    filteredReferance = filteredReferance.OrderBy(r => r.Description);
                    break;
                case 3:
                    filteredReferance = filteredReferance.OrderBy(r => r.ReferanceTitle);
                    break;
                case 4:
                    filteredReferance = filteredReferance.OrderBy(r => r.Referanceİmage);
                    break;
                default:
                    filteredReferance = filteredReferance.OrderBy(r => r.ReferanceId);
                    break;
            }
        }
        var displayedReferance = filteredReferance.Skip(displayStart);
        if (displayLength >= 0)
        {
            displayedReferance = displayedReferance.Take(displayLength);
        }
        totalRecords = allReferances.Count();
        totalDisplayRecords = filteredReferance.Count();
        return displayedReferance.ToList();

    }
}
public interface IReferanceRepository : IRepository<Referance>
{
    IEnumerable<Referance> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
}
}
