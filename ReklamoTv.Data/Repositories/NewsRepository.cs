﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{
   public class NewsRepository : RepositoryBase<News>, INewsRepository
    {

        public NewsRepository(IDbFactory dbFactory)  
                : base(dbFactory) { }



        public IEnumerable<News> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.Newses.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(n=>n.NewsTitle.Contains(sSearch) || n.Description.Contains(sSearch) || n.NewsTime.ToString().Contains(sSearch));

                }      
            }

            var allNewses = query;

            IEnumerable<News> filteredNews = allNewses;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredNews = filteredNews.OrderBy( n=>n.NewsId);
                        break;
                    case 2:
                        filteredNews = filteredNews.OrderBy(n=>n.NewsTime);
                        break;
                    case 3:
                        filteredNews = filteredNews.OrderBy(n=>n.NewsTitle);
                        break;
                    case 4:
                        filteredNews = filteredNews.OrderBy(n=>n.Description);
                        break;
                    case 5:
                        filteredNews = filteredNews.OrderBy(n => n.NewsSpot);
                        break;
                    case 6:
                        filteredNews = filteredNews.OrderBy(n => n.StartTime);
                        break;
                    case 7:
                        filteredNews = filteredNews.OrderBy(n => n.ExpiryTime);
                        break;

                    default:
                        filteredNews = filteredNews.OrderBy(n=>n.NewsId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredNews = filteredNews.OrderBy(n => n.NewsId);
                        break;
                    case 2:
                        filteredNews = filteredNews.OrderBy(n => n.NewsTime);
                        break;
                    case 3:
                        filteredNews = filteredNews.OrderBy(n => n.NewsTitle);
                        break;
                    case 4:
                        filteredNews = filteredNews.OrderBy(n => n.Description);
                        break;
                    case 5:
                        filteredNews = filteredNews.OrderBy(n => n.NewsSpot);
                        break;
                    case 6:
                        filteredNews = filteredNews.OrderBy(n => n.StartTime);
                        break;
                    case 7:
                        filteredNews = filteredNews.OrderBy(n => n.ExpiryTime);
                        break;

                    default:
                        filteredNews = filteredNews.OrderBy(n => n.NewsId);
                        break;
                }
            }
            var displayedNews = filteredNews.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedNews = displayedNews.Take(displayLength);
            }
            totalRecords = allNewses.Count();
            totalDisplayRecords = filteredNews.Count();
            return displayedNews.ToList();

        }
    }
    public interface INewsRepository : IRepository<News>
    {
        IEnumerable<News> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }
}
