﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{

    public class ContactRepository : RepositoryBase<Contact>, IContactRepository
    {
        public ContactRepository(IDbFactory dbFactory)
            : base(dbFactory) { }



        public IEnumerable<Contact> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.Contacts.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(l => l.ContactAdress.Contains(sSearch) || l.ContactPhone.Contains(sSearch) || l.FacebookAdress.Contains(sSearch) || l.FirstEMail.Contains(sSearch) || l.GoogleAdress.Contains(sSearch) || l.LinkedAdress.Contains(sSearch) || l.Longtitude.Contains(sSearch) || l.SecondEMail.Contains(sSearch) || l.TwitterAdress.Contains(sSearch) || l.Latitude.Contains(sSearch) || l.SkypeAdress.Contains(sSearch));

                }
            }

            var allContacts = query;

            IEnumerable<Contact> filteredContacts = allContacts;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactId);
                        break;
                    case 2:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactAdress);
                        break;
                    case 3:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactPhone);
                        break;
                    case 4:
                        filteredContacts = filteredContacts.OrderBy(l => l.FacebookAdress);
                        break;
                    case 5:
                        filteredContacts = filteredContacts.OrderBy(l => l.GoogleAdress);
                        break;
                    case 6:
                        filteredContacts = filteredContacts.OrderBy(l => l.LinkedAdress);
                        break;
                    case 7:
                        filteredContacts = filteredContacts.OrderBy(l => l.FirstEMail);
                        break;
                    case 8:
                        filteredContacts = filteredContacts.OrderBy(l => l.SecondEMail);
                        break;
                    case 9:
                        filteredContacts = filteredContacts.OrderBy(l => l.TwitterAdress);
                        break;
                    case 10:
                        filteredContacts = filteredContacts.OrderBy(l => l.SkypeAdress);
                        break;
                    case 11:
                        filteredContacts = filteredContacts.OrderBy(l => l.Latitude);
                        break;
                    case 12:
                        filteredContacts = filteredContacts.OrderBy(l => l.Longtitude);
                        break;
                    default:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactId);
                        break;
                    case 2:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactAdress);
                        break;
                    case 3:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactPhone);
                        break;
                    case 4:
                        filteredContacts = filteredContacts.OrderBy(l => l.FacebookAdress);
                        break;
                    case 5:
                        filteredContacts = filteredContacts.OrderBy(l => l.GoogleAdress);
                        break;
                    case 6:
                        filteredContacts = filteredContacts.OrderBy(l => l.LinkedAdress);
                        break;
                    case 7:
                        filteredContacts = filteredContacts.OrderBy(l => l.FirstEMail);
                        break;
                    case 8:
                        filteredContacts = filteredContacts.OrderBy(l => l.SecondEMail);
                        break;
                    case 9:
                        filteredContacts = filteredContacts.OrderBy(l => l.TwitterAdress);
                        break;
                    case 10:
                        filteredContacts = filteredContacts.OrderBy(l => l.SkypeAdress);
                        break;
                    case 11:
                        filteredContacts = filteredContacts.OrderBy(l => l.Latitude);
                        break;
                    case 12:
                        filteredContacts = filteredContacts.OrderBy(l => l.Longtitude);
                        break;
                    default:
                        filteredContacts = filteredContacts.OrderBy(l => l.ContactId);
                        break;
                }
            }
            var displayedContactes = filteredContacts.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedContactes = displayedContactes.Take(displayLength);
            }
            totalRecords = allContacts.Count();
            totalDisplayRecords = filteredContacts.Count();
            return displayedContactes.ToList();

        }
    }
    public interface IContactRepository : IRepository<Contact>
    {
        IEnumerable<Contact> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }
}


