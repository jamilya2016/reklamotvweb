﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{

    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {

        public TagRepository(IDbFactory dbFactory)
            : base(dbFactory) { }

        public IEnumerable<Tag> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.Tags.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(t => t.TagTitle.Contains(sSearch));
                }
            }

            var allTags = query;

            IEnumerable<Tag> filteredTags = allTags;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredTags = filteredTags.OrderBy(t => t.TagId);
                        break;
                    case 2:
                        filteredTags = filteredTags.OrderBy(t => t.TagTitle);
                        break;
                    case 3:
                        filteredTags = filteredTags.OrderBy(t => t.Tags);
                        break;
                    case 4:
                        filteredTags = filteredTags.OrderBy(t => t.DescriptionTag);
                        break;

                    default:
                        filteredTags = filteredTags.OrderBy(t => t.TagId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredTags = filteredTags.OrderByDescending(t => t.TagId);
                        break;
                    case 2:
                        filteredTags = filteredTags.OrderByDescending(t => t.Tags);
                        break;
                    case 3:
                        filteredTags = filteredTags.OrderByDescending(t => t.TagTitle);
                        break;
                    case 4:
                        filteredTags = filteredTags.OrderByDescending(t => t.DescriptionTag);
                        break;

                    default:
                        filteredTags = filteredTags.OrderByDescending(t => t.TagId);
                        break;
                }
            }
            var displayedTags = filteredTags.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedTags = displayedTags.Take(displayLength);
            }
            totalRecords = allTags.Count();
            totalDisplayRecords = filteredTags.Count();
            return displayedTags.ToList();

        }
    }
    public interface ITagRepository : IRepository<Tag>
    {
        IEnumerable<Tag> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }
}

