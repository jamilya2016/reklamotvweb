﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{
    class ContactForm1Repository:RepositoryBase<ContactForm1>, IContactForm1Repository
    {
        public ContactForm1Repository(IDbFactory dbFactory)
                : base(dbFactory) { }

        public IEnumerable<ContactForm1> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.ContactForm1s.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(f=>f.ContactName.Contains(sSearch));

                }
            }

            var allContactForm1s = query;

            IEnumerable<ContactForm1> filteredContactForm1 = allContactForm1s;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f=>f.ContactFormId);
                        break;
                    case 2:
                        filteredContactForm1 =filteredContactForm1.OrderBy(f =>f.ContactMail);
                        break;
                    case 3:
                       filteredContactForm1 = filteredContactForm1.OrderBy(f=>f.ContactName);
                        break;
                    case 4:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f =>f.ContactSurname);
                        break;

                    case 5:
                        filteredContactForm1= filteredContactForm1.OrderBy(f => f.ContactTelefon);
                        break;
                    case 6:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactMesaj);
                        break;
                    default:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f =>f.ContactFormId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {

                    case 1:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactFormId);
                        break;
                    case 2:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactMail);
                        break;
                    case 3:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactName);
                        break;
                    case 4:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactSurname);
                        break;

                    case 5:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactTelefon);
                        break;
                    case 6:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactMesaj);
                        break;
                    default:
                        filteredContactForm1 = filteredContactForm1.OrderBy(f => f.ContactFormId);
                        break;
                }
            }
            var displayedContactForm1 = filteredContactForm1.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedContactForm1 = displayedContactForm1.Take(displayLength);
            }
            totalRecords = allContactForm1s.Count();
            totalDisplayRecords = filteredContactForm1.Count();
            return displayedContactForm1.ToList();

        }
    }
    public interface IContactForm1Repository : IRepository<ContactForm1>
    {
        IEnumerable<ContactForm1> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }
}


   
