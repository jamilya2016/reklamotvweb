﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{
    public class AboutUsRepository : RepositoryBase<AboutUs>, IAboutUsRepository
    {

        public AboutUsRepository(IDbFactory dbFactory)
                : base(dbFactory) { }

        public IEnumerable<AboutUs> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.AboutUses.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(a => a.Description.Contains(sSearch));

                }
            }

            var allAboutUses = query;

            IEnumerable<AboutUs> filteredAboutUs = allAboutUses;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutId);
                        break;
                    case 2:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.Description);
                        break;
                    case 3:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutTitle);
                        break;


                    default:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutId);
                        break;
                    case 2:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.Description);
                        break;
                    case 3:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutTitle);
                        break;

                    default:
                        filteredAboutUs = filteredAboutUs.OrderBy(a => a.AboutId);
                        break;
                }
            }
            var displayedAboutUs = filteredAboutUs.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedAboutUs = displayedAboutUs.Take(displayLength);
            }
            totalRecords = allAboutUses.Count();
            totalDisplayRecords = filteredAboutUs.Count();
            return displayedAboutUs.ToList();

        }
    }
    public interface IAboutUsRepository : IRepository<AboutUs>
    {
        IEnumerable<AboutUs> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }
}


