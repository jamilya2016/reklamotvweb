﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Data.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDbFactory dbFactory)
                : base(dbFactory) { }



        public IEnumerable<Product> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            search = search.Trim();
            var searchWords = search.Split(' ');


            var query = this.DbContext.Products.AsQueryable();
            foreach (string sSearch in searchWords)
            {
                if (sSearch != null && sSearch != "")
                {
                    query = query.Where(p => p.Description.Contains(sSearch) || p.Title.Contains(sSearch));

                }
            }

            var allProduct = query;

            IEnumerable<Product> filteredProduct = allProduct;

            if (sortDirection == "asc")
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredProduct = filteredProduct.OrderBy(p => p.ProductId);
                        break;
                    case 2:
                        filteredProduct = filteredProduct.OrderBy(p => p.Description);
                        break;
                    case 3:
                        filteredProduct = filteredProduct.OrderBy(p => p.Title);
                        break;
                    case 4:
                        filteredProduct = filteredProduct.OrderBy(p => p.Contents);
                        break;

                    default:
                        filteredProduct = filteredProduct.OrderBy(p => p.ProductId);
                        break;
                }
            }
            else
            {
                switch (sortColumnIndex)
                {
                    case 1:
                        filteredProduct = filteredProduct.OrderBy(p => p.ProductId);
                        break;
                    case 2:
                        filteredProduct = filteredProduct.OrderBy(p => p.Description);
                        break;
                    case 3:
                        filteredProduct = filteredProduct.OrderBy(p => p.Title);
                        break;
                    case 4:
                        filteredProduct = filteredProduct.OrderBy(p => p.Contents);
                        break;

                    default:
                        filteredProduct = filteredProduct.OrderBy(p => p.ProductId);
                        break;
                }
            }
            var displayedProduct = filteredProduct.Skip(displayStart);
            if (displayLength >= 0)
            {
                displayedProduct = displayedProduct.Take(displayLength);
            }
            totalRecords = allProduct.Count();
            totalDisplayRecords = filteredProduct.Count();
            return displayedProduct.ToList();

        }
    }
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
    }

}

