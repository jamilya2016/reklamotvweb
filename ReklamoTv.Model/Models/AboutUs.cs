﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
     public class AboutUs
    {
        public int AboutId { get; set; }
        public  string Description { get; set; }
        public string AboutTitle { get; set; }

    }
}
