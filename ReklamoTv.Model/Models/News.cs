﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
   public class News
    {
        public int NewsId { get; set; }
        public string   NewsTitle { get; set; }
        public string  Description { get; set; }
        public string NewsSpot { get; set; }
        public DateTime NewsTime{ get; set; }
        public DateTime StartTime { get; set; }
        public DateTime ExpiryTime { get; set; }


    }
}
