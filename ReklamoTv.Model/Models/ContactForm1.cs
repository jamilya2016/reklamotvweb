﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
     public class ContactForm1
    {
        public int ContactFormId { get; set; }
        public  string ContactName { get; set; }
        public string ContactSurname { get; set; }
        public string ContactTelefon  { get; set; }
        public string  ContactMesaj { get; set; }
        public string ContactMail { get; set; }
    }
}
