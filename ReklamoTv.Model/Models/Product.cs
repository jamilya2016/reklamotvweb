﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
     public class Product
    {
        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }

    }
}
