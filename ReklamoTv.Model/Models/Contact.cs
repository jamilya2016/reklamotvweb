﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string ContactAdress { get; set; }
        public string ContactPhone { get; set; }
        public string FirstEMail { get; set; }
        public string SecondEMail { get; set; }
        public string SkypeAdress { get; set; }
        public string FacebookAdress { get; set; }
        public string TwitterAdress { get; set; }
        public string LinkedAdress { get; set; }
        public string GoogleAdress { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }

    }
}
