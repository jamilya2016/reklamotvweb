﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Model
{
    public class Tag
    {
        public int TagId { get; set; }
        public string TagTitle { get; set; }
        public string DescriptionTag { get; set; }
        public string Tags { get; set; }
        
       
    }

}
