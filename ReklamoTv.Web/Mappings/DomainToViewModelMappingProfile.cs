﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Web.ViewModels;

namespace ReklamoTv.Web.Mappings
{
    public class DomainToViewModelMappingProfile:Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

#pragma warning disable CS0672 // Member overrides obsolete member
        protected override void Configure()
#pragma warning restore CS0672 // Member overrides obsolete member
        {
            Mapper.CreateMap<AboutUs, AboutUsViewModel>();
            Mapper.CreateMap<Tag, TagViewModel>();
            Mapper.CreateMap<News, NewsViewModel>();
            Mapper.CreateMap<Product, ProductViewModel>();
            Mapper.CreateMap<Contact, ContactViewModel>();
            Mapper.CreateMap<Referance, ReferanceViewModel>();
            Mapper.CreateMap<ContactForm1, ContactForm1ViewModel>();
        }
    }
}
