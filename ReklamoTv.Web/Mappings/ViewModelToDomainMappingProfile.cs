﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Web.ViewModels;

namespace ReklamoTv.Web.Mappings
{
    public class ViewModelToDomainMappingProfile:Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

#pragma warning disable CS0672 // Member overrides obsolete member
        protected override void Configure()
#pragma warning restore CS0672 // Member overrides obsolete members
        {
            Mapper.CreateMap<AboutUsFormViewModel, AboutUs>();
            Mapper.CreateMap<NewsFormViewModel, News>();
            Mapper.CreateMap<ProductFormViewModel, Product>();
            Mapper.CreateMap<TagFormViewModel, Tag>();
            Mapper.CreateMap<ReferanceFormViewModel, Referance>();
            Mapper.CreateMap<ContactFormViewModel, Contact>();
            Mapper.CreateMap<ContactForm1FromViewModel, ContactForm1>();
        }
    }
}
