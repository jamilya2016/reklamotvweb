﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class ContactForm1FromViewModel
    {
        public int ContactFormId { get; set; }
        [DisplayName("Musteri Adı")]
        public string ContactName { get; set; }
        [DisplayName("Musteri Soyadi")]
        public string ContactSurname { get; set; }
        [DisplayName("Musteri Telefon No")]
        public string ContactTelefon { get; set; }
        [DisplayName("Musteri Mesaji")]
        public string ContactMesaj { get; set; }
        [DisplayName("Musteri E-Mail")]
        public string ContactMail { get; set; }
    }
}