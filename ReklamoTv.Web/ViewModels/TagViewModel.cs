﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class TagViewModel
    {
        public int TagId { get; set; }
        [DisplayName("Başlık")]
        public string TagTitle { get; set; }
        [DisplayName("Açıklama")]
        public string DescriptionTag { get; set; }
        [DisplayName("Etiket")]
        public string Tags { get; set; }

    }
}