﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class ContactViewModel
    {
        public int ContactId { get; set; }
        [DisplayName("Adres")]
        public string ContactAdress { get; set; }
        [DisplayName("Telefon")]
        public string ContactPhone { get; set; }
        [Required(ErrorMessage = "Lütfen E-mail adresi giriniz")]
        [DisplayName("E-mail 1")]
        public string FirstEMail { get; set; }
        [DisplayName("E-mail 2")]
        public string SecondEMail { get; set; }
        [DisplayName("Skype")]
        public string SkypeAdress { get; set; }
        [DisplayName("Facebook")]
        public string FacebookAdress { get; set; }
        [DisplayName("Twitter")]
        public string TwitterAdress { get; set; }
        [DisplayName("Linked")]
        public string LinkedAdress { get; set; }
        [DisplayName("Google")]
        public string GoogleAdress { get; set; }
        [DisplayName("Enlem")]
        public string Latitude { get; set; }
        [DisplayName("Boylam")]
        public string Longtitude { get; set; }

    }
}