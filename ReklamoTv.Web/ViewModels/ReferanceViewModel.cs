﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class ReferanceViewModel
    {
        public int ReferanceId { get; set; }
        [DisplayName("Başlık")]
        public string ReferanceTitle { get; set; }
        [DisplayName("Logo")]
        public string Referanceİmage { get; set; }
        [DisplayName("Açıklama")]
        public string Description { get; set; }
    }
}
