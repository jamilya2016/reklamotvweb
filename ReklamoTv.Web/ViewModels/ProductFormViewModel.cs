﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class ProductFormViewModel
    {
        public int ProductId { get; set; }
        [DisplayName("Başlık")]
        public string Title { get; set; }
        [DisplayName("İletişim")]
        public string Contents { get; set; }
        [DisplayName("Açıklama")]
        public string Description { get; set; }

    }
}