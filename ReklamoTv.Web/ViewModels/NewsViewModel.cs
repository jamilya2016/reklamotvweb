﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class NewsViewModel
    {

        public int NewsId { get; set; }
        [DisplayName("Başlık")]
        public string NewsTitle { get; set; }
        [DisplayName("Açıklama")]
        public string Description { get; set; }
        [DisplayName("Eklenme Tarih")]
        public DateTime NewsTime { get; set; }
        [DisplayName(" Haber ekleme başlangıç tarihi")]
        public DateTime StartTime { get; set; }
        [DisplayName("Haber ekleme bitiş tarihi")]
        public DateTime ExpiryTime { get; set; }
        [DisplayName("Haber spotu")]
        public string NewsSpot { get; set; }

    }
}