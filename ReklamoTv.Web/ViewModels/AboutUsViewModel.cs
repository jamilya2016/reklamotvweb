﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class AboutUsViewModel
    {
        public int AboutId { get; set; }
        [DisplayName("Açıklama")]
        [Required(ErrorMessage ="Lütfen açıklama giriniz")]
        public string Description { get; set; }
        [DisplayName("Başlık")]
        [Required(ErrorMessage = "Lütfen başlık giriniz")]
        public string AboutTitle { get; set; }
    }
}