﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReklamoTv.Web.ViewModels
{
    public class NewsFormViewModel
    {

        public int NewsId { get; set; }
        [DisplayName("Başlık")]
        [Required(ErrorMessage ="Lütfen başlık giriniz")]
        public string NewsTitle { get; set; }
        [DisplayName("Açıklama")]
        [Required(ErrorMessage =" Lütfen açıklama giriniz")]
        public string Description { get; set; }
        [DisplayName("Eklenme Tarih")]
        public DateTime? NewsTime { get; set; }
        [DisplayName(" Haber ekleme başlangıç tarihi")]
        [Required(ErrorMessage ="Ekleme başlangıç tarihi giriniz ")]
        public DateTime StartTime { get; set; }
        [Required(ErrorMessage = "Ekleme tarihi giriniz")]
        [DisplayName ("Haber ekleme bitiş tarihi")]
        public  DateTime ExpiryTime { get; set; }
        [DisplayName ("Haber spotu")]
        public string NewsSpot { get; set; }

    }
}