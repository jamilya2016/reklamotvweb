﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReklamoTv.Web.Startup))]
namespace ReklamoTv.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
