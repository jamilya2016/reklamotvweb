﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class ReferanceController:Controller
    {
        private readonly IReferanceService referanceService;
        protected readonly string UploadPath;

        public ReferanceController(IReferanceService referanceService)
        {
            this.referanceService = referanceService;
            this.UploadPath = ConfigurationManager.AppSettings["UploadPath"];
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ReferanceFormViewModel referanceForm, System.Web.HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                var referance= Mapper.Map<ReferanceFormViewModel, Referance>(referanceForm);
                if (upload != null)
                {

                            string filePath = Path.GetFileName(upload.FileName);

                            var uploadAddress = Path.Combine(Server.MapPath("~/ReferanceFile"), filePath);
                            upload.SaveAs(uploadAddress);
                            referance.Referanceİmage = upload.FileName;
      
                }
                referanceService.CreateReferance(referance);
                referanceService.SaveReferance();
                return RedirectToAction("Index");
            }
            return View(referanceForm);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var displayedReferances = referanceService.Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from r in displayedReferances
                         select new[] { r.ReferanceId.ToString(), r.Description, r.ReferanceTitle,r.Referanceİmage, string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var referance = referanceService.GetReferance(id.Value);
                if (referance != null)
                {
                    var referanceViewModel = Mapper.Map<Referance, ReferanceViewModel>(referance);
                    return View(referanceViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ReferanceFormViewModel referanceForm)
        {
            if (ModelState.IsValid)
            {
                var referance = Mapper.Map<ReferanceFormViewModel, Referance>(referanceForm);
                referanceService.UpdateReferance(referance);
                referanceService.SaveReferance();
                return RedirectToAction("Index");
            }
            return View(referanceForm);
        }
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                var referance = referanceService.GetReferance(id.Value);
                if (referance != null)
                {
                    referanceService.DeleteReferance(referance);
                    referanceService.SaveReferance();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
