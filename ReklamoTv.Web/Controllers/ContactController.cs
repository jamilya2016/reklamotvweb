﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class ContactController:Controller
    {

        private readonly IContactService contactService;

        public ContactController(IContactService contactService)
        {
            this.contactService = contactService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactFormViewModel contactForm)
        {
            if (ModelState.IsValid)
            {
               
                var contact = Mapper.Map<ContactFormViewModel, Contact>(contactForm);

                contactService.CreateContact(contact);
                contactService.SaveContact();
                return RedirectToAction("Index");
            }
            return View(contactForm);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var displayedContacts = contactService.Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from c in displayedContacts
                         select new[] { c.ContactId.ToString(), c.ContactAdress,c.ContactPhone,c.FirstEMail,string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int ? id)
        {
            if (id.HasValue)
            {
                var contact = contactService.GetContact(id.Value);
                if (contact != null)
                {
                    var contactViewModel = Mapper.Map<Contact, ContactViewModel>(contact);
                    return View(contactViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactFormViewModel contactForm)
        {
            if (ModelState.IsValid)
            {
                var contact = Mapper.Map<ContactFormViewModel,Contact>(contactForm);
                contactService.UpdateContact(contact);
                contactService.SaveContact();
                return RedirectToAction("Index");
            }
            return View(contactForm);
        }
        public ActionResult Delete( int? id)
        {
            if (id.HasValue)
            {
                var contact =contactService.GetContact(id.Value);
                if (contact != null)
                {
                   contactService.DeleteContact(contact);
                    contactService.SaveContact();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
