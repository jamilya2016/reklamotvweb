﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class TagController:Controller
    {
        private readonly ITagService tagService;

        public TagController(ITagService tagService)
        {
            this.tagService = tagService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TagFormViewModel tagForm)
        {
            if (ModelState.IsValid)
            {
               
                var city = Mapper.Map<TagFormViewModel, Tag>(tagForm);

                tagService.CreateTag(city);
                tagService.SaveTag();
                return RedirectToAction("Index");
            }
            return View(tagForm);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var displayedTags = tagService.Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from t in displayedTags
                         select new[] { t.TagId.ToString(), t.DescriptionTag, t.TagTitle,t.Tags,string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit( int ? id)
        {
            if (id.HasValue)
            {
                var tag = tagService.GetTag(id.Value);
                if (tag != null)
                {
                    var tagViewModel = Mapper.Map<Tag, TagViewModel>(tag);
                    return View(tagViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TagFormViewModel tagForm)
        {
            if (ModelState.IsValid)
            {
                var tag = Mapper.Map<TagFormViewModel, Tag>(tagForm);
                tagService.UpdateTag(tag);
                tagService.SaveTag();
                return RedirectToAction("Index");
            }
            return View(tagForm);
        }
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                var tag = tagService.GetTag(id.Value);
                if (tag != null)
                {
                    tagService.DeleteTag(tag);
                    tagService.SaveTag();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
