﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class NewsController:Controller
    {
        private readonly INewsService newsService;

        public NewsController(INewsService newsService)
        {
            this.newsService = newsService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewsFormViewModel newsForm)
        {
            if (ModelState.IsValid)
            {
                var news = Mapper.Map<NewsFormViewModel, News>(newsForm);

               newsService.CreateNews(news);
                newsService.SaveNews();
                return RedirectToAction("Index");
            }
            return View(newsForm);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var displayedNewses = newsService.Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from n in displayedNewses
                         select new[] { n.NewsId.ToString(), n.NewsTitle,n.NewsTime.ToString(), string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit( int? id)
        {
            if (id.HasValue)
            {
                var news = newsService.GetNews(id.Value);
                if (news != null)
                {
                    var newsViewModel = Mapper.Map<News, NewsViewModel>(news);
                    return View(newsViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewsFormViewModel newsForm)
        {
            if (ModelState.IsValid)
            {
                var city = Mapper.Map<NewsFormViewModel, News>( newsForm);
                newsService.UpdateNews(city);
                newsService.SaveNews();
                return RedirectToAction("Index");
            }
            return View(newsService);
        }
        public ActionResult Delete(int ? id)
        {
            if (id.HasValue)
            {
                var news = newsService.GetNews(id.Value);
                if (news != null)
                {
                    newsService.DeleteNews(news);
                    newsService.SaveNews();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
