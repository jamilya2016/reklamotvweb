﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class ProductController: Controller
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductFormViewModel productForm)
        {
            if (ModelState.IsValid)
            {
                
                var product = Mapper.Map<ProductFormViewModel, Product>(productForm);

                productService.CreateProduct(product);
                productService.SaveProduct();
                return RedirectToAction("Index");
            }
            return View(productForm);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var displayedProducts =productService .Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from  p in displayedProducts
                         select new[] { p.ProductId.ToString(), p.Description,p.Contents,p.Title, string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit( int ? id)
        {
            if (id.HasValue)
            {
                var product= productService.GetProduct(id.Value);
                if (product != null)
                {
                    var productViewModel = Mapper.Map<Product, ProductViewModel>(product);
                    return View(productViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductFormViewModel productForm)
        {
            if (ModelState.IsValid)
            {
                var product = Mapper.Map<ProductFormViewModel, Product>(productForm);
                productService.UpdateProduct(product);
                productService.SaveProduct();
                return RedirectToAction("Index");
            }
            return View(productForm);
        }
        public ActionResult Delete(int ? id)
        {
            if (id.HasValue)
            {
                var product = productService.GetProduct(id.Value);
                if (product != null)
                {
                    productService.DeleteProduct(product);
                    productService.SaveProduct();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
