﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class AboutUsController:Controller
    {
        private readonly IAboutUsService aboutusService;
           
        public AboutUsController(IAboutUsService aboutusService)
        {
            this.aboutusService = aboutusService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AboutUsFormViewModel aboutusForm)
        {
            if (ModelState.IsValid)
            {
                
                var aboutus = Mapper.Map<AboutUsFormViewModel, AboutUs>(aboutusForm);

                aboutusService.CreateAboutUs(aboutus);
                aboutusService.SaveAboutUs();
                return RedirectToAction("Index");
            }
            return View(aboutusForm);
        }
       


        public ActionResult Edit(int ? id)
        {
            if (id.HasValue)
            {
                var abotus = aboutusService.GetAboutUs(id.Value);
                if (abotus != null)
                {
                    var aboutusViewModel = Mapper.Map<AboutUs, AboutUsViewModel>(abotus);
                    return View(aboutusViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AboutUsFormViewModel aboutusForm)
        {
            if (ModelState.IsValid)
            {
                var aboutus = Mapper.Map<AboutUsFormViewModel, AboutUs>(aboutusForm);
                aboutusService.UpdateAboutUs(aboutus);
                aboutusService.SaveAboutUs();
                return RedirectToAction("Index");
            }
            return View(aboutusForm);
        }
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                var aboutus =aboutusService.GetAboutUs(id.Value);
                if (aboutus != null)
                {
                    aboutusService.DeleteAboutUs(aboutus);
                    aboutusService.SaveAboutUs();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
