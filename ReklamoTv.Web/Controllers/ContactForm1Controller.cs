﻿using AutoMapper;
using ReklamoTv.Model;
using ReklamoTv.Service;
using ReklamoTv.Web.Models;
using ReklamoTv.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ReklamoTv.Web.Controllers
{
    public class ContactForm1Controller:Controller
    {
        private readonly IContactForm1Service contactForm1Service;

        public ContactForm1Controller(IContactForm1Service contactService)
        {
            this.contactForm1Service = contactService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ContactForm1FromViewModel contactForm)
        {
            if (ModelState.IsValid)
            {

                var contact = Mapper.Map<ContactForm1FromViewModel, ContactForm1>(contactForm);

                contactForm1Service.CreateContactForm1(contact);
                contactForm1Service.SaveContactForm1();
                return RedirectToAction("Index");
            }
            return View(contactForm);
        }



        public ActionResult Edit(int? id)
        {
            if (id.HasValue)
            {
                var contactForm1 = contactForm1Service.GetContactForm1(id.Value);
                if (contactForm1 != null)
                {
                    var contactForm1ViewModel = Mapper.Map<ContactForm1,ContactForm1ViewModel>(contactForm1);
                    return View(contactForm1ViewModel);
                }

            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactForm1FromViewModel contactForm1)
        {
            if (ModelState.IsValid)
            {
                var contactForm = Mapper.Map<ContactForm1FromViewModel, ContactForm1>(contactForm1);
                contactForm1Service.UpdateContactForm1(contactForm);
                contactForm1Service.SaveContactForm1();
                return RedirectToAction("Index");
            }
            return View(contactForm1);
        }
        public ActionResult AjaxHandler(jQueryDataTableParamModel param)
        {
            string sSearch = "";
            if (param.sSearch != null) sSearch = param.sSearch;
            var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            var sortDirection = Request["sSortDir_0"]; // asc or desc
            int iTotalRecords;
            int iTotalDisplayRecords;
            var   displayContactForm1=contactForm1Service.Search(sSearch, sortColumnIndex, sortDirection, param.iDisplayStart, param.iDisplayLength, out iTotalRecords, out iTotalDisplayRecords);

            var result = from f in displayContactForm1
                         select new[] { f.ContactFormId.ToString(),f.ContactName,f.ContactSurname,f.ContactTelefon,f.ContactMail,f.ContactMesaj, string.Empty };
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = iTotalRecords,
                iTotalDisplayRecords = iTotalDisplayRecords,
                aaData = result.ToList()
            },
                JsonRequestBehavior.AllowGet);

        }
        public ActionResult Delete(int? id)
        {
            if (id.HasValue)
            {
                var contactf = contactForm1Service.GetContactForm1(id.Value);
                if( contactf != null)
                {
                    contactForm1Service.DeleteContactForm1(contactf);
                    contactForm1Service.SaveContactForm1();
                    return RedirectToAction("Index");
                }

            }
            return HttpNotFound();
        }


    }
}
