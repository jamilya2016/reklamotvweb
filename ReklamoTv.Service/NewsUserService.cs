﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
   public interface INewsService
    {
        IEnumerable<News> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<News> GetNewses();
        News GetNews(int id);
        void CreateNews(News city);
        void UpdateNews(News city);
        void DeleteNews(News city);
        void SaveNews();
    }
    public  class NewsUserService :INewsService
    {
        private readonly INewsRepository newsRepository;
        private readonly IUnitOfWork unitOfWork;
        public NewsUserService(INewsRepository newsRepository, IUnitOfWork unitOfWork)
        {
            this.newsRepository = newsRepository;
            this.unitOfWork = unitOfWork;
        }
        #region ICityService Members
        public IEnumerable<News> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var newses =newsRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return newses;

        }
        public IEnumerable<News> GetNewses()
        {
            var newses = newsRepository.GetAll();
            return newses;
        }
        public  News GetNews(int id)
        {
            var news = newsRepository.GetById(id);
            return news;
        }
        public void CreateNews(News news)
        {
           newsRepository.Add(news);
        }
        public void UpdateNews(News news)
        {
            newsRepository.Update(news);
        }
        public void DeleteNews(News news)
        {
            newsRepository.Delete(news);
        }
        public void SaveNews()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}

