﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
    public interface ITagService
    {
        IEnumerable<Tag> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<Tag> GetTags();
        Tag GetTag(int id);
        void CreateTag(Tag tag);
        void UpdateTag(Tag tag);
        void DeleteTag(Tag tag);
        void SaveTag();
    }
    public class TagService : ITagService
    {
        private readonly ITagRepository tagRepository;
        private readonly IUnitOfWork unitOfWork;
        public TagService(ITagRepository tagRepository, IUnitOfWork unitOfWork)
        {
            this.tagRepository = tagRepository;
            this.unitOfWork = unitOfWork;
        }
        #region ITagService Members
        public IEnumerable<Tag> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var tags = tagRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return tags;

        }
        public IEnumerable<Tag> GetTags()
        {
            var tags = tagRepository.GetAll();
            return tags;
        }
        public Tag GetTag(int id)
        {
            var tag = tagRepository.GetById(id);
            return tag;
        }
        public void CreateTag(Tag tag)
        {
            tagRepository.Add(tag);
        }
        public void UpdateTag(Tag tag)
        {
            tagRepository.Update(tag);
        }
        public void DeleteTag(Tag tag)
        {
            tagRepository.Delete(tag);
        }
        public void SaveTag()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}

