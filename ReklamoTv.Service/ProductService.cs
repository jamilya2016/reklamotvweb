﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
   public interface IProductService
    {
        IEnumerable<Product> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<Product> GetProducts();
        Product GetProduct(int id);
        void CreateProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
        void SaveProduct();
    }
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;
        private readonly IUnitOfWork unitOfWork;
        public ProductService(IProductRepository productRepository, IUnitOfWork unitOfWork)
        {
            this.productRepository = productRepository;
            this.unitOfWork = unitOfWork;
        }
        #region IProductService Members
        public IEnumerable<Product> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var products =productRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return products;

        }
        public IEnumerable<Product> GetProducts()
        {
            var products = productRepository.GetAll();
            return products;
        }
        public Product GetProduct(int id)
        {
            var product = productRepository.GetById(id);
            return product;
        }
        public void CreateProduct(Product product)
        {
            productRepository.Add(product);
        }
        public void UpdateProduct(Product product)

        {
            productRepository.Update(product);
        }
        public void DeleteProduct(Product product)
        {
            productRepository.Delete(product);
        }
        public void SaveProduct()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}

