﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
    public interface IContactForm1Service
    {

        IEnumerable<ContactForm1> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<ContactForm1> GetContactForm1s();
        ContactForm1 GetContactForm1(int id);
        void CreateContactForm1(ContactForm1 contactForm);
        void UpdateContactForm1(ContactForm1 contactForm);
        void DeleteContactForm1(ContactForm1 contactForm);
        void SaveContactForm1();
    }
    public class ContactForm1Service : IContactForm1Service
    {
        private readonly IContactForm1Repository contactForm1Repository;
        private readonly IUnitOfWork unitOfWork;
        public ContactForm1Service(IContactForm1Repository contactForm1Repository, IUnitOfWork unitOfWork)
        {
            this.contactForm1Repository = contactForm1Repository;
            this.unitOfWork = unitOfWork;
        }
        #region IContactForm1Service Members
        public IEnumerable<ContactForm1> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var contacts = contactForm1Repository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return contacts;

        }
        public IEnumerable<ContactForm1> GetContactForm1s()
        {
            var contacts = contactForm1Repository.GetAll();
            return contacts;
        }
        public ContactForm1 GetContactForm1(int id)
        {
            var contactForm = contactForm1Repository.GetById(id);
            return contactForm;
        }
        public void CreateContactForm1(ContactForm1 contactForm)
        {
            contactForm1Repository.Add(contactForm);
        }
        public void UpdateContactForm1(ContactForm1 contactForm)
        {
            contactForm1Repository.Update(contactForm);
        }
        public void DeleteContactForm1(ContactForm1 contactForm)
        {
            contactForm1Repository.Delete(contactForm);
        }
        public void SaveContactForm1()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}



