﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
   
        public interface IContactService
    {
            IEnumerable<Contact> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
            IEnumerable<Contact> GetContacts();
            Contact GetContact(int id);
            void CreateContact(Contact contact);
            void UpdateContact(Contact contact);
            void DeleteContact(Contact contact);
            void SaveContact();
        }
        public class ContactService : IContactService
        {
            private readonly IContactRepository contactRepository;
            private readonly IUnitOfWork unitOfWork;
            public ContactService(IContactRepository contactRepository, IUnitOfWork unitOfWork)
            {
                this.contactRepository = contactRepository;
                this.unitOfWork = unitOfWork;
            }
            #region IContactService Members
            public IEnumerable<Contact> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
            {
                var contacts = contactRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

                return contacts;

            }
            public IEnumerable<Contact> GetContacts()
            {
                var contacts = contactRepository.GetAll();
                return contacts;
            }
            public Contact GetContact(int id)
            {
                var contact= contactRepository.GetById(id);
                return contact;
            }
            public void CreateContact(Contact contact)
            {
                contactRepository.Add(contact);
            }
            public void UpdateContact(Contact contact)
            {
                contactRepository.Update(contact);
            }
            public void DeleteContact(Contact contact)
            {
                contactRepository.Delete(contact);
            }
            public void SaveContact()
            {
                unitOfWork.Commit();
            }
            #endregion
        }
    }

