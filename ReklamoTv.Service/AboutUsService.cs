﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
   public interface IAboutUsService
    {

        IEnumerable<AboutUs> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<AboutUs> GetAboutUses();
        AboutUs GetAboutUs( int id);
        void CreateAboutUs(AboutUs aboutUs);
        void UpdateAboutUs(AboutUs aboutUs);
        void DeleteAboutUs(AboutUs aboutUs);
        void SaveAboutUs();
    }
    public class AboutUsService : IAboutUsService
    {
        private readonly IAboutUsRepository aboutUsRepository;
        private readonly IUnitOfWork unitOfWork;
        public AboutUsService(IAboutUsRepository aboutUsRepository, IUnitOfWork unitOfWork)
        {
            this.aboutUsRepository = aboutUsRepository;
            this.unitOfWork = unitOfWork;
        }
        #region IAboutUsService Members
        public IEnumerable<AboutUs> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var aboutUses = aboutUsRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return aboutUses;

        }
        public IEnumerable<AboutUs> GetAboutUses()
        {
            var aboutUs = aboutUsRepository.GetAll();
            return aboutUs;
        }
        public AboutUs GetAboutUs(int id)
        {
            var aboutUs = aboutUsRepository.GetById(id);
            return aboutUs;
        }
        public void CreateAboutUs(AboutUs aboutUs)
        {
            aboutUsRepository.Add(aboutUs);
        }
        public void UpdateAboutUs(AboutUs aboutUs)
        {
            aboutUsRepository.Update(aboutUs);
        }
        public void DeleteAboutUs(AboutUs aboutUs)
        {
            aboutUsRepository.Delete(aboutUs);
        }
        public void SaveAboutUs()
        {
            unitOfWork.Commit();
        }
        #endregion
    }
}

