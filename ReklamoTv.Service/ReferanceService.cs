﻿using ReklamoTv.Data.Infrastructure;
using ReklamoTv.Data.Repositories;
using ReklamoTv.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReklamoTv.Service
{
    public interface IReferanceService
    {
        IEnumerable<Referance> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords);
        IEnumerable<Referance> GetReferances();
        Referance GetReferance(int id);
        void CreateReferance(Referance referance);
        void UpdateReferance(Referance aboutUs);
        void DeleteReferance(Referance aboutUs);
        void SaveReferance();
    }
    public class ReferanceService : IReferanceService
    {
        private readonly IReferanceRepository referanceRepository;
        private readonly IUnitOfWork unitOfWork;
        public ReferanceService(IReferanceRepository referanceRepository, IUnitOfWork unitOfWork)
        {
            this.referanceRepository = referanceRepository;
            this.unitOfWork = unitOfWork;
        }
        #region IAboutUsService Members
        public IEnumerable<Referance> Search(string search, int sortColumnIndex, string sortDirection, int displayStart, int displayLength, out int totalRecords, out int totalDisplayRecords)
        {
            var referances = referanceRepository.Search(search, sortColumnIndex, sortDirection, displayStart, displayLength, out totalRecords, out totalDisplayRecords);

            return referances;

        }
        public IEnumerable<Referance> GetReferances()
        {
            var  referance = referanceRepository.GetAll();
            return referance;
        }
        public Referance GetReferance(int id)
        {
            var referance = referanceRepository.GetById(id);
            return referance;
        }
        public void CreateReferance(Referance referance)
        {
            referanceRepository.Add(referance);
        }
        public void UpdateReferance(Referance referance)
        {
            referanceRepository.Update(referance);
        }
        public void DeleteReferance(Referance referance)
        {
           referanceRepository.Delete(referance);
        }
        public void SaveReferance()
        {
            unitOfWork.Commit();
        }
        #endregion
    }

}
